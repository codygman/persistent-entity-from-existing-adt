{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
module MinimalRepro where

import           Control.Monad.IO.Class
import           Control.Monad.Logger
import           Data.Text              as T
import           Database.Persist
import           Database.Persist.MySQL
import           Database.Persist.TH

data Person = Person { name :: String
                     , age  :: Int
                     } deriving (Eq, Read, Show)

derivePersistField "Person"
-- This gives me:
-- instance PersistField Person
-- instance PersistFieldSql Person

-- Maybe this type error means I have to satisfy the PersistEntity typeclass?

addToDB :: Person -> IO ()
addToDB entity = do
  runNoLoggingT $ do
    withMySQLPool defaultConnectInfo 10 $ \pool -> do
      liftIO $ flip runSqlPersistMPool pool $ do
        insert (entity :: Person)
        return ()

-- Error:
-- Compilation failed.
-- src/MinimalRepro.hs:31:9-14: Couldn't match expected type ‘PersistEntityBackend Person’ …
--                 with actual type ‘SqlBackend’
--     In a stmt of a 'do' block: insert (entity :: Person)
--     In the second argument of ‘($)’, namely
--       ‘do { insert (entity :: Person);
--             return () }’
--     In the second argument of ‘($)’, namely
--       ‘flip runSqlPersistMPool pool
--        $ do { insert (entity :: Person);
--               return () }’
-- Compilation failed.
